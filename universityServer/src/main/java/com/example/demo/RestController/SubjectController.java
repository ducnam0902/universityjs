package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Subject;
import com.example.demo.Services.SubjectServices;

@RestController
@RequestMapping(path = "/subject", produces = "application/json")
@CrossOrigin(origins = "*")
public class SubjectController {
	private SubjectServices subjectServices;

	public SubjectController(SubjectServices subjectServices) {
		this.subjectServices = subjectServices;
	}

	// Show all Subject
	@GetMapping
	public List<Subject> findAll() {
		return subjectServices.findAll();
	}

	// Add new Subject
	@PostMapping
	public void addSubject(@RequestBody Subject Subject) {
		subjectServices.save(Subject);
	}

	@PutMapping(value = "update/{id}")
	public void updateSubject(@RequestBody Subject Subject) {
		subjectServices.save(Subject);
	}

	// Delete Subject
	@DeleteMapping(value = "/delete/{id}")
	public void deleteSubject(@PathVariable("id") Long id) {
		subjectServices.remove(id);
	}

	@GetMapping(value = "/search/{keyword}")
	public List<Subject> searchSubject(@PathVariable("keyword") String keyword) {
		return subjectServices.search(keyword);
	}
}
