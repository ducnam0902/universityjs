package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Study;
import com.example.demo.Services.StudyServices;

@RestController
@RequestMapping(path = "/study", produces = "application/json")
@CrossOrigin(origins = "*")
public class StudyController {
	private StudyServices studyServices;
	public StudyController(StudyServices studyServices) {
		this.studyServices= studyServices;
	}
	
	//Show all Study
	@GetMapping
	public List<Study> findAll(){
		return studyServices.findAll();
	}
	//Add new study
		@PostMapping
		public void addstudy(@RequestBody Study study) {
			studyServices.save(study);
		}
		
		@PutMapping(value="update/{id}")
		public void updatestudy( @RequestBody Study study) {
			studyServices.save(study);
		}
		
		//Delete study
		@DeleteMapping(value="/delete/{id}")
		public void deletestudy(@PathVariable("id")Long id) {
			studyServices.remove(id);
		}
		
		@GetMapping(value = "/search/{keyword}")
		public List<Study> searchstudy(@PathVariable("keyword")String keyword){
			return studyServices.search(keyword);
		}
}
