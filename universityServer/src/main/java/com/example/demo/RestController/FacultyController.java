package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Faculty;
import com.example.demo.Services.FacultyServices;

@RestController
@RequestMapping(path = "/faculty", produces = "application/json")
@CrossOrigin(origins = "*")
public class FacultyController {
	private FacultyServices facultyServices;
	public FacultyController(FacultyServices facultyServices ) {
		this.facultyServices = facultyServices;
	}
	
	//Show all Faculty
	@GetMapping
	public List<Faculty> findAll(){
		return facultyServices.findAll();
	}
	
	//Add new Faculty
	@PostMapping
	public void addFaculty(@RequestBody Faculty faculty) {
		facultyServices.save(faculty);
	}
	
	@PutMapping(value="update/{id}")
	public void updateFaculty( @RequestBody Faculty faculty) {
		facultyServices.save(faculty);
	}
	
	//Delete faculty
	@DeleteMapping(value="/delete/{id}")
	public void deleteFaculty(@PathVariable("id")Long id) {
		facultyServices.remove(id);
	}
	
	@GetMapping(value = "/search/{keyword}")
	public List<Faculty> searchFaculty(@PathVariable("keyword")String keyword){
		return facultyServices.search(keyword);
	}
	
}
