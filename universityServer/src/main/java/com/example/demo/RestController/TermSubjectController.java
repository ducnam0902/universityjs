package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.TermSubject;
import com.example.demo.Services.TermSubjectServices;

@RestController
@RequestMapping(path = "/termsubject", produces = "application/json")
@CrossOrigin(origins = "*")
public class TermSubjectController {
	private TermSubjectServices tSS;

	public TermSubjectController(TermSubjectServices tSS) {
		this.tSS = tSS;
	}

	// Show all TermSubjectService
	@GetMapping
	public List<TermSubject> findAll() {
		return tSS.findAll();
	}

	// Add new TermSubject
	@PostMapping
	public void addTermSubject(@RequestBody TermSubject TermSubject) {
		tSS.save(TermSubject);
	}

	@PutMapping(value = "update/{id}")
	public void updateTermSubject(@RequestBody TermSubject TermSubject) {
		tSS.save(TermSubject);
	}

	// Delete TermSubject
	@DeleteMapping(value = "/delete/{id}")
	public void deleteTermSubject(@PathVariable("id") Long id) {
		tSS.remove(id);
	}

	@GetMapping(value = "/search/{keyword}")
	public List<TermSubject> searchTermSubject(@PathVariable("keyword") String keyword) {
		return tSS.search(keyword);
	}
}
