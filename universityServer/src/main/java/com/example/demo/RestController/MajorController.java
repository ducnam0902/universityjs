package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.demo.Model.Major;
import com.example.demo.Services.MajorServices;

@RestController
@RequestMapping(path = "/major", produces = "application/json")
@CrossOrigin(origins = "*")
public class MajorController {
	private MajorServices majorServices;

	public MajorController(MajorServices majorServices) {
		this.majorServices = majorServices;
	}

	// Show all Major
	@GetMapping
	public List<Major> findAll() {
		return majorServices.findAll();
	}

	// Add new Major
	@PostMapping
	public void addMajor(@RequestBody Major major) {
		majorServices.save(major);
	}

	@PutMapping(value = "update/{id}")
	public void updateFaculty(@RequestBody Major major) {
		majorServices.save(major);
	}

	// Delete Major
	@DeleteMapping(value = "/delete/{id}")
	public void deleteFaculty(@PathVariable("id") Long id) {
		majorServices.remove(id);
	}

	@GetMapping(value = "/search/{keyword}")
	public List<Major> searchFaculty(@PathVariable("keyword") String keyword) {
		return majorServices.search(keyword);
	}
}
