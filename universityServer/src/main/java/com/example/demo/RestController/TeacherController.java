package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Teacher;
import com.example.demo.Services.TeacherServices;

@RestController
@RequestMapping(path = "/teacher", produces = "application/json")
@CrossOrigin(origins = "*")
public class TeacherController {
	private TeacherServices teacherServices;

	public TeacherController(TeacherServices teacherServices) {
		this.teacherServices = teacherServices;
	}

	// Show all Teacher
	@GetMapping
	public List<Teacher> findAll() {
		return teacherServices.findAll();
	}

	// Add new Teacher
	@PostMapping
	public void addTeacher(@RequestBody Teacher Teacher) {
		teacherServices.save(Teacher);
	}

	@PutMapping(value = "update/{id}")
	public void updateTeacher(@RequestBody Teacher Teacher) {
		teacherServices.save(Teacher);
	}

	// Delete Teacher
	@DeleteMapping(value = "/delete/{id}")
	public void deleteTeacher(@PathVariable("id") Long id) {
		teacherServices.remove(id);
	}

	@GetMapping(value = "/search/{keyword}")
	public List<Teacher> searchTeacher(@PathVariable("keyword") String keyword) {
		return teacherServices.search(keyword);
	}
}
