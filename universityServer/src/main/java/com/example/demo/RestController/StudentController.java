package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Student;
import com.example.demo.Services.StudentServices;

@RestController
@RequestMapping(path = "/student", produces = "application/json")
@CrossOrigin(origins = "*")
public class StudentController {
	private StudentServices studentServices;

	public StudentController(StudentServices studentServices) {
		this.studentServices = studentServices;
	}

	// Show all Score
	@GetMapping
	public List<Student> findAll() {
		return studentServices.findAll();
	}

	// Add new Student
	@PostMapping
	public void addStudent(@RequestBody Student Student) {
		studentServices.save(Student);
	}

	@PutMapping(value = "update/{id}")
	public void updateStudent(@RequestBody Student Student) {
		studentServices.save(Student);
	}

	// Delete Student
	@DeleteMapping(value = "/delete/{id}")
	public void deleteStudent(@PathVariable("id") Long id) {
		studentServices.remove(id);
	}

	@GetMapping(value = "/search/{keyword}")
	public List<Student> searchStudent(@PathVariable("keyword") String keyword) {
		return studentServices.search(keyword);
	}
}
