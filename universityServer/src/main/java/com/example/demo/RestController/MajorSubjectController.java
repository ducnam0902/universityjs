package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.MajorSubject;
import com.example.demo.Services.MajorSubjectServices;

@RestController
@RequestMapping(path = "/majorsubject", produces = "application/json")
@CrossOrigin(origins = "*")
public class MajorSubjectController {
	private MajorSubjectServices mSS;

	public MajorSubjectController(MajorSubjectServices mSS) {
		this.mSS = mSS;
	}

	// Show all Major Subject
	@GetMapping
	public List<MajorSubject> findAll() {
		return mSS.findAll();
	}

	// Add new MajorSubject
	@PostMapping
	public void addMajorSubject(@RequestBody MajorSubject MajorSubject) {
		mSS.save(MajorSubject);
	}

	@PutMapping(value = "update/{id}")
	public void updateMajorSubject(@RequestBody MajorSubject MajorSubject) {
		mSS.save(MajorSubject);
	}

	// Delete MajorSubject
	@DeleteMapping(value = "/delete/{id}")
	public void deleteMajorSubject(@PathVariable("id") Long id) {
		mSS.remove(id);
	}

	@GetMapping(value = "/search/{keyword}")
	public List<MajorSubject> searchMajorSubject(@PathVariable("keyword") String keyword) {
		return mSS.search(keyword);
	}

}
