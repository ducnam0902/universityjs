package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Term;
import com.example.demo.Services.TermServices;

@RestController
@RequestMapping(path = "/term", produces = "application/json")
@CrossOrigin(origins = "*")
public class TermController {
	private TermServices termServices;
	public TermController(TermServices termServices ) {
		this.termServices= termServices;
	}
	
	//Show all Term
	@GetMapping
	public List<Term> findAll(){
		return termServices.findAll();
	}
	//Add new Term
		@PostMapping
		public void addTerm(@RequestBody Term term) {
			termServices.save(term);
		}
		
		@PutMapping(value="update/{id}")
		public void updateTerm( @RequestBody Term term) {
			termServices.save(term);
		}
		
		//Delete Term
		@DeleteMapping(value="/delete/{id}")
		public void deleteTerm(@PathVariable("id")Long id) {
			termServices.remove(id);
		}
		
		@GetMapping(value = "/search/{keyword}")
		public List<Term> searchTerm(@PathVariable("keyword")String keyword){
			return termServices.search(keyword);
		}
}
