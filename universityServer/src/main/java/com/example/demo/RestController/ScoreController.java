package com.example.demo.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Score;
import com.example.demo.Services.ScoreServices;

@RestController
@RequestMapping(path = "/score", produces = "application/json")
@CrossOrigin(origins = "*")
public class ScoreController {
	private ScoreServices scoreServices;

	public ScoreController(ScoreServices scoreServices) {
		this.scoreServices = scoreServices;
	}

	// Show all Score
	@GetMapping
	public List<Score> findAll() {
		return scoreServices.findAll();
	}

	// Add new Score
	@PostMapping
	public void addScore(@RequestBody Score Score) {
		scoreServices.save(Score);
	}

	@PutMapping(value = "update/{id}")
	public void updateScore(@RequestBody Score Score) {
		scoreServices.save(Score);
	}

	// Delete Score
	@DeleteMapping(value = "/delete/{id}")
	public void deleteScore(@PathVariable("id") Long id) {
		scoreServices.remove(id);
	}

	@GetMapping(value = "/search/{keyword}")
	public List<Score> searchScore(@PathVariable("keyword") String keyword) {
		return scoreServices.search(keyword);
	}
}
