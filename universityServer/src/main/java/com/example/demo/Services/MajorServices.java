package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Major;

public interface MajorServices {
	List<Major> findAll();

	public void save(Major m);

	public void remove(Long id);

	public Optional<Major> findById(Long id);

	public List<Major> search(String keyword);
}
