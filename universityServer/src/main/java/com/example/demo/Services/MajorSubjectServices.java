package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.MajorSubject;

public interface MajorSubjectServices {
	List<MajorSubject> findAll();
	
	public void save(MajorSubject m);

	public void remove(Long id);

	public Optional<MajorSubject> findById(Long id);

	public List<MajorSubject> search(String keyword);
}
