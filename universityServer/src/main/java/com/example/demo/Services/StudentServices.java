package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Student;

public interface StudentServices {
	List<Student> findAll();
	
	public void save(Student s);

	public void remove(Long id);

	public Optional<Student> findById(Long id);

	public List<Student> search(String keyword);
}
