package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Study;

public interface StudyServices {
	List<Study> findAll();
	
	public void save(Study s);

	public void remove(Long id);

	public Optional<Study> findById(Long id);

	public List<Study> search(String keyword);
	
}
