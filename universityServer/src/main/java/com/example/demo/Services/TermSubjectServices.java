package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.TermSubject;

public interface TermSubjectServices {
	List<TermSubject> findAll();
	
	public void save(TermSubject t);

	public void remove(Long id);

	public Optional<TermSubject> findById(Long id);

	public List<TermSubject> search(String keyword);
	
}
