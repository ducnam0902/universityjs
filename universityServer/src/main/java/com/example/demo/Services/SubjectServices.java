package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Subject;

public interface SubjectServices {
	List<Subject> findAll();
	
	public void save(Subject s);

	public void remove(Long id);

	public Optional<Subject> findById(Long id);

	public List<Subject> search(String keyword);
	
}
