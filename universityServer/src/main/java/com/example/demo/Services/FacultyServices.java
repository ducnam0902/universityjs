package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Faculty;

public interface FacultyServices {
	List<Faculty> findAll();
	
	public void save(Faculty f);
	
	public void remove(Long id);
	
	public Optional<Faculty> findById( Long id);
	
	public List<Faculty> search( String keyword);
	
}
