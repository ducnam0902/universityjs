package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Score;

public interface ScoreServices {
	List<Score> findAll();
	
	public void save(Score s);

	public void remove(Long id);

	public Optional<Score> findById(Long id);

	public List<Score> search(String keyword);
}
