package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Term;

public interface TermServices {
	List<Term> findAll();
	
	public void save(Term t);

	public void remove(Long id);

	public Optional<Term> findById(Long id);

	public List<Term> search(String keyword);
	
}
