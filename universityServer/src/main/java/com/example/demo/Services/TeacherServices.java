package com.example.demo.Services;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Teacher;

public interface TeacherServices {
	List<Teacher> findAll();
	
	public void save(Teacher t);

	public void remove(Long id);

	public Optional<Teacher> findById(Long id);

	public List<Teacher> search(String keyword);
	
}
