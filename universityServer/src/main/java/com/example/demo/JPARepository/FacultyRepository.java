package com.example.demo.JPARepository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.Model.Faculty;
@Repository
public interface FacultyRepository extends JpaRepository<Faculty, Long> {

	@Query(value = "SELECT * FROM FACULTY WHERE FACULTY_NAME LIKE %?1% OR ADDRESS LIKE %?1% OR PHONE LIKE %?1%", nativeQuery = true)
	public List<Faculty> FacultySearch(String keyword);
}
