package com.example.demo.JPARepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Model.MajorSubject;

@Repository
public interface MajorSubjectRepository extends JpaRepository<MajorSubject, Long> {

}
