package com.example.demo.JPARepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Model.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long>{

}
