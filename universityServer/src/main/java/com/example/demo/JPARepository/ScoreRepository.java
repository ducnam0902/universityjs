package com.example.demo.JPARepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.Model.Score;

@Repository
public interface ScoreRepository extends JpaRepository<Score, Long> {

}
