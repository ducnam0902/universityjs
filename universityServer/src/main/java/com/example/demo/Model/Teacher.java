package com.example.demo.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Teacher {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long teacherId;
	private String teacherName;
	private String address;
	private String phone;
	private String certificate;
	
	//Relationship with Faculty 
	@ManyToOne
	@JoinColumn(name= "faculty_id")
	private Faculty faculty;

	
	//Relationship with Term Subject
	@OneToMany(mappedBy="teacher")
	private List<TermSubject> termSubject;
	
	
	
	public Teacher(Long teacherId, String teacherName, String address, String phone, String certificate,
			Faculty faculty) {
		super();
		this.teacherId = teacherId;
		this.teacherName = teacherName;
		this.address = address;
		this.phone = phone;
		this.certificate = certificate;
		this.faculty = faculty;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}
	
	
}
