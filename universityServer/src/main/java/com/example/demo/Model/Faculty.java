package com.example.demo.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Faculty {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long facultyId;
	private String facultyName;

	private String address;

	private String phone;
	
	//Relationship with Major
	@OneToMany(mappedBy ="faculty")
	private List<Major> major;
	
	
	//Relationship with Subject
	@OneToMany(mappedBy ="faculty")
	private List<Subject> subject;
	
	
	public Faculty(Long facultyId, String facultyName, String address, String phone) {
		super();
		this.facultyId = facultyId;
		this.facultyName = facultyName;
		this.address = address;
		this.phone = phone;
	}
	
	
	//Getter setter
	public Long getFacultyId() {
		return facultyId;
	}
	public void setFacultyId(Long facultyId) {
		this.facultyId = facultyId;
	}
	public String getFacultyName() {
		return facultyName;
	}
	public void setFacultyName(String facultyName) {
		this.facultyName = facultyName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}


	public Faculty() {
		super();
	}
	
	
}
