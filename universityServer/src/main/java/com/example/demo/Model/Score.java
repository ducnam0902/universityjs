package com.example.demo.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Score {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long scoreId;
	
	@ManyToOne
	@JoinColumn(name="study_id")
	private Study study;
	
	private float scoreOne;
	private float scoreTwo;
	private float scoreThree;
	private float midScore;
	private float testScore;
	public Score(Long scoreId, Study study, float scoreOne, float scoreTwo, float scoreThree, float midScore,
			float testScore) {
		super();
		this.scoreId = scoreId;
		this.study = study;
		this.scoreOne = scoreOne;
		this.scoreTwo = scoreTwo;
		this.scoreThree = scoreThree;
		this.midScore = midScore;
		this.testScore = testScore;
	}
	public Long getScoreId() {
		return scoreId;
	}
	public void setScoreId(Long scoreId) {
		this.scoreId = scoreId;
	}
	public Study getStudy() {
		return study;
	}
	public void setStudy(Study study) {
		this.study = study;
	}
	public float getScoreOne() {
		return scoreOne;
	}
	public void setScoreOne(float scoreOne) {
		this.scoreOne = scoreOne;
	}
	public float getScoreTwo() {
		return scoreTwo;
	}
	public void setScoreTwo(float scoreTwo) {
		this.scoreTwo = scoreTwo;
	}
	public float getScoreThree() {
		return scoreThree;
	}
	public void setScoreThree(float scoreThree) {
		this.scoreThree = scoreThree;
	}
	public float getMidScore() {
		return midScore;
	}
	public void setMidScore(float midScore) {
		this.midScore = midScore;
	}
	public float getTestScore() {
		return testScore;
	}
	public void setTestScore(float testScore) {
		this.testScore = testScore;
	}
	
	
	
}
