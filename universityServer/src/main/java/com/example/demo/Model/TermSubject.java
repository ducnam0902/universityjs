package com.example.demo.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class TermSubject {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long termSubjectId;
	
	//Relationship with term
	@ManyToOne
	@JoinColumn( name="term_id")
	private Term term;
	
	//Relationship with subject
	@ManyToOne
	@JoinColumn( name="subject_id")
	private Subject subject;
	
	//Relationship with subject
	@ManyToOne
	@JoinColumn( name="teacher_id")
	private Teacher teacher;
	
	//Relationship with study
	@OneToMany(mappedBy="termSubject")
	private List<Study> study;
	
	private String classroom;
	
	private String timeSchedule;

	public TermSubject(Long termSubjectId, Term term, Subject subject, Teacher teacher, String classroom,
			String timeSchedule) {
		super();
		this.termSubjectId = termSubjectId;
		this.term = term;
		this.subject = subject;
		this.teacher = teacher;
		this.classroom = classroom;
		this.timeSchedule = timeSchedule;
	}

	public Long getTermSubjectId() {
		return termSubjectId;
	}

	public void setTermSubjectId(Long termSubjectId) {
		this.termSubjectId = termSubjectId;
	}

	public Term getTerm() {
		return term;
	}

	public void setTerm(Term term) {
		this.term = term;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public String getClassroom() {
		return classroom;
	}

	public void setClassroom(String classroom) {
		this.classroom = classroom;
	}

	public String getTimeSchedule() {
		return timeSchedule;
	}

	public void setTimeSchedule(String timeSchedule) {
		this.timeSchedule = timeSchedule;
	}
	
	
	
}
