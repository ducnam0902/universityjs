package com.example.demo.Model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Term {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long termId;
	private String termName;
	private Date startedDate;
	private Date endDate;
	
	//Relationship with TermSubject
	@OneToMany(mappedBy="term")
	private List<TermSubject> termSubject;
	
	
	public Term(Long termId, String termName, Date startedDate, Date endDate) {
		super();
		this.termId = termId;
		this.termName = termName;
		this.startedDate = startedDate;
		this.endDate = endDate;
	}


	//Getter setter
	public Long getTermId() {
		return termId;
	}



	public void setTermId(Long termId) {
		this.termId = termId;
	}



	public String getTermName() {
		return termName;
	}



	public void setTermName(String termName) {
		this.termName = termName;
	}



	public Date getStartedDate() {
		return startedDate;
	}



	public void setStartedDate(Date startedDate) {
		this.startedDate = startedDate;
	}



	public Date getEndDate() {
		return endDate;
	}



	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
}
