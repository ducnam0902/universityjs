package com.example.demo.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Major {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long majorId;
	private String majorName;
	
	//Relationship with faculty
	@ManyToOne
	@JoinColumn(name="faculty_id")
	private Faculty faculty;
	
	// Relationship with MajorSubject
	@OneToMany(mappedBy = "major")
	List<MajorSubject> majorSubject;
	
	public Major(Long majorId, String majorName, Faculty faculty) {
		super();
		this.majorId = majorId;
		this.majorName = majorName;
		this.faculty = faculty;
	}
	
	public Major() {
		super();
	}

	//Get and set
	public Long getMajorId() {
		return majorId;
	}

	public void setMajorId(Long majorId) {
		this.majorId = majorId;
	}

	public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}
	
	
	 
}
