package com.example.demo.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Study {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long studyId;
	
	
	@ManyToOne
	@JoinColumn(name="student_id")
	private Student student;
	
	
	@ManyToOne
	@JoinColumn(name="term_subject_id")
	private TermSubject termSubject;

	//Relationship with score
	@OneToMany(mappedBy="study")
	private List<Score> score;

	public Study(Long studyId, Student student, TermSubject termSubject) {
		super();
		this.studyId = studyId;
		this.student = student;
		this.termSubject = termSubject;
	}


	public Long getStudyId() {
		return studyId;
	}


	public void setStudyId(Long studyId) {
		this.studyId = studyId;
	}


	public Student getStudent() {
		return student;
	}


	public void setStudent(Student student) {
		this.student = student;
	}


	public TermSubject getTermSubject() {
		return termSubject;
	}


	public void setTermSubject(TermSubject termSubject) {
		this.termSubject = termSubject;
	}
	
	
}
