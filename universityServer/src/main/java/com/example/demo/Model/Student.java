package com.example.demo.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;



@Entity
public class Student {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long studentId;
	private String studentName;
	private String address;
	private String phone;
	private String startedYear;
	
	//Relationship with major
	@OneToOne
	@JoinColumn( name= "major_id")
	private Major major;

	
	//Relationship with study
	@OneToMany(mappedBy ="student")
	private List<Study> study;
	
	public Student(Long studentId, String studentName, String address, String phone, String startedYear,
			Major major) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.address = address;
		this.phone = phone;
		this.startedYear = startedYear;
		this.major = major;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStartedYear() {
		return startedYear;
	}

	public void setStartedYear(String startedYear) {
		this.startedYear = startedYear;
	}

	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}
	
	
}
