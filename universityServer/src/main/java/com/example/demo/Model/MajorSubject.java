package com.example.demo.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class MajorSubject {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long majorSubjectId;
	
	//Relationship with major
	@ManyToOne
	@JoinColumn(name="major_id")
	private Major major;
	
	
	//Relationship with major
	@ManyToOne
	@JoinColumn(name="subject_id")
	private Subject subject;


	public MajorSubject(Long majorSubjectId, Major major, Subject subject) {
		super();
		this.majorSubjectId = majorSubjectId;
		this.major = major;
		this.subject = subject;
	}


	public Long getMajorSubjectId() {
		return majorSubjectId;
	}


	public void setMajorSubjectId(Long majorSubjectId) {
		this.majorSubjectId = majorSubjectId;
	}


	public Major getMajor() {
		return major;
	}


	public void setMajor(Major major) {
		this.major = major;
	}


	public Subject getSubject() {
		return subject;
	}


	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	
	
}
