package com.example.demo.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Subject {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long subjectId;
	private String subjectName;
	private long credit;
	
	// Relationship with Faculty
	@ManyToOne
	@JoinColumn(name="faculty_id")
	private Faculty faculty;
	
	// Relationship with MajorSubject
	@OneToMany(mappedBy = "subject")
	private List<MajorSubject> majorSubject;
	
	//Relationship with TermSubject
	@OneToMany(mappedBy = "subject")
	private List<TermSubject> termSubject;

	public Subject(Long subjectId, String subjectName, long credit, Faculty faculty) {
		super();
		this.subjectId = subjectId;
		this.subjectName = subjectName;
		this.credit = credit;
		this.faculty = faculty;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public long getCredit() {
		return credit;
	}

	public void setCredit(long credit) {
		this.credit = credit;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}
	
	
	
}
