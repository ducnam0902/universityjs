package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.MajorSubjectRepository;
import com.example.demo.Model.MajorSubject;
import com.example.demo.Services.MajorSubjectServices;

@Service
public class MajorSubjectServicesImpl implements MajorSubjectServices {

	@Autowired
	MajorSubjectRepository mSRepo;

	@Override
	public List<MajorSubject> findAll() {
		// TODO Auto-generated method stub
		return mSRepo.findAll();
	}

	@Override
	public void save(MajorSubject m) {
		// TODO Auto-generated method stub
		mSRepo.save(m);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		mSRepo.deleteById(id);
	}

	@Override
	public Optional<MajorSubject> findById(Long id) {
		// TODO Auto-generated method stub
		return mSRepo.findById(id);
	}

	@Override
	public List<MajorSubject> search(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
