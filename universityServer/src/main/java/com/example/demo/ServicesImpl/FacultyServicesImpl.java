package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.FacultyRepository;
import com.example.demo.Model.Faculty;
import com.example.demo.Services.FacultyServices;

@Service
public class FacultyServicesImpl implements FacultyServices {
	@Autowired
	FacultyRepository falRepo;

	@Override
	public List<Faculty> findAll() {		
		return falRepo.findAll();
	}

	@Override
	public void save(Faculty f) {
		falRepo.save(f);
		
	}

	@Override
	public void remove(Long id) {	
		falRepo.deleteById(id);
		
	}

	@Override
	public Optional<Faculty> findById(Long id) {
		return falRepo.findById(id);
	}

	@Override
	public List<Faculty> search(String keyword) {
		
		return falRepo.FacultySearch(keyword);
	}

	
}
