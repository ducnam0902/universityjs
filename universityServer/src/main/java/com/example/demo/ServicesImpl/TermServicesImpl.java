package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.TermRepository;
import com.example.demo.Model.Term;
import com.example.demo.Services.TermServices;

@Service
public class TermServicesImpl implements TermServices {
	@Autowired
	TermRepository termRepo;

	@Override
	public List<Term> findAll() {
		// TODO Auto-generated method stub
		return termRepo.findAll();
	}

	@Override
	public void save(Term t) {
		// TODO Auto-generated method stub
		termRepo.save(t);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		termRepo.deleteById(id);
	}

	@Override
	public Optional<Term> findById(Long id) {
		// TODO Auto-generated method stub
		return termRepo.findById(id);
	}

	@Override
	public List<Term> search(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}

}
