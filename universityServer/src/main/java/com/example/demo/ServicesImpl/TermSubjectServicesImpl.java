package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.TermSubjectRepository;
import com.example.demo.Model.TermSubject;
import com.example.demo.Services.TermSubjectServices;

@Service
public class TermSubjectServicesImpl implements TermSubjectServices {
	@Autowired
	TermSubjectRepository tSRepo;

	@Override
	public List<TermSubject> findAll() {
		// TODO Auto-generated method stub
		return tSRepo.findAll();
	}

	@Override
	public void save(TermSubject t) {
		// TODO Auto-generated method stub
		tSRepo.save(t);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		tSRepo.deleteById(id);
	}

	@Override
	public Optional<TermSubject> findById(Long id) {
		// TODO Auto-generated method stub
		return tSRepo.findById(id);
	}

	@Override
	public List<TermSubject> search(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}
}
