package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.ScoreRepository;
import com.example.demo.Model.Score;
import com.example.demo.Services.ScoreServices;

@Service
public class ScoreServicesImpl implements ScoreServices {
	@Autowired
	ScoreRepository scoreRepo;

	@Override
	public List<Score> findAll() {
		// TODO Auto-generated method stub
		return scoreRepo.findAll();
	}

	@Override
	public void save(Score s) {
		// TODO Auto-generated method stub
		scoreRepo.save(s);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		scoreRepo.deleteById(id);
	}

	@Override
	public Optional<Score> findById(Long id) {
		// TODO Auto-generated method stub
		return scoreRepo.findById(id);
	}

	@Override
	public List<Score> search(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
