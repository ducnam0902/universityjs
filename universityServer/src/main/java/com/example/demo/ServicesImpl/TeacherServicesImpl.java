package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.TeacherRepository;
import com.example.demo.Model.Teacher;
import com.example.demo.Services.TeacherServices;

@Service
public class TeacherServicesImpl implements TeacherServices {
	@Autowired
	TeacherRepository teacherRepo;

	@Override
	public List<Teacher> findAll() {
		// TODO Auto-generated method stub
		return teacherRepo.findAll();
	}

	@Override
	public void save(Teacher t) {
		// TODO Auto-generated method stub
		teacherRepo.save(t);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		teacherRepo.deleteById(id);
	}

	@Override
	public Optional<Teacher> findById(Long id) {
		// TODO Auto-generated method stub
		return teacherRepo.findById(id);
	}

	@Override
	public List<Teacher> search(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}

}
