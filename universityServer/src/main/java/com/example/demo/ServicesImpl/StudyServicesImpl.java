package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.StudyRepository;
import com.example.demo.Model.Study;
import com.example.demo.Services.StudyServices;

@Service
public class StudyServicesImpl implements StudyServices{
 @Autowired
 StudyRepository studyRepo;

@Override
public List<Study> findAll() {
	// TODO Auto-generated method stub
	return studyRepo.findAll();
}

@Override
public void save(Study s) {
	// TODO Auto-generated method stub
	studyRepo.save(s);
}

@Override
public void remove(Long id) {
	// TODO Auto-generated method stub
	studyRepo.deleteById(id);
}

@Override
public Optional<Study> findById(Long id) {
	// TODO Auto-generated method stub
	return studyRepo.findById(id);
}

@Override
public List<Study> search(String keyword) {
	// TODO Auto-generated method stub
	return null;
}
 
}
