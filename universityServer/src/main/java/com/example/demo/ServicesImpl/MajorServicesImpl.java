package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.MajorRepository;
import com.example.demo.Model.Major;
import com.example.demo.Services.MajorServices;



@Service
public class MajorServicesImpl implements MajorServices {
	
	@Autowired
	MajorRepository majorRepo;

	@Override
	public List<Major> findAll() {
		return majorRepo.findAll();
	}

	@Override
	public void save(Major m) {
		majorRepo.save(m);
	}

	@Override
	public void remove(Long id) {
		majorRepo.deleteById(id);;
	}

	@Override
	public Optional<Major> findById(Long id) {
		return majorRepo.findById(id);
	}

	@Override
	public List<Major> search(String keyword) {
		return null;
	}

}
