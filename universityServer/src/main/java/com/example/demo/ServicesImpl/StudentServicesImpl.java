package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.StudentRepository;
import com.example.demo.Model.Student;
import com.example.demo.Services.StudentServices;

@Service
public class StudentServicesImpl implements StudentServices {
	@Autowired
	StudentRepository stuRepo;

	@Override
	public List<Student> findAll() {
		// TODO Auto-generated method stub
		return stuRepo.findAll();
	}

	@Override
	public void save(Student s) {
		// TODO Auto-generated method stub
		stuRepo.save(s);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		stuRepo.deleteById(id);
	}

	@Override
	public Optional<Student> findById(Long id) {
		// TODO Auto-generated method stub
		return stuRepo.findById(id);
	}

	@Override
	public List<Student> search(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}

}
