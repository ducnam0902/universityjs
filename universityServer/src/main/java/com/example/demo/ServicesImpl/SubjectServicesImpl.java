package com.example.demo.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.JPARepository.SubjectRepository;
import com.example.demo.Model.Subject;
import com.example.demo.Services.SubjectServices;

@Service
public class SubjectServicesImpl implements SubjectServices {
	@Autowired
	SubjectRepository subRepo;

	@Override
	public List<Subject> findAll() {
		// TODO Auto-generated method stub
		return subRepo.findAll();
	}

	@Override
	public void save(Subject s) {
		// TODO Auto-generated method stub
		subRepo.save(s);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		subRepo.deleteById(id);
	}

	@Override
	public Optional<Subject> findById(Long id) {
		// TODO Auto-generated method stub
		return subRepo.findById(id);
	}

	@Override
	public List<Subject> search(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}

}
