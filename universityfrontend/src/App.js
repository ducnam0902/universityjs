import './App.css';
import { BrowserRouter as Router} from "react-router-dom";
import Direction from './Route/Direction';
import Footer from './Components/Common/Footer';
import Header from './Components/Common/Header';
function App() {
  return (
    <Router>
    <Header/>
    <Direction/>
    <Footer/>
  </Router>
  );
}

export default App;
