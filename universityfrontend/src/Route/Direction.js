import React from "react";
import { Route, Switch } from "react-router-dom";
import Slide from "../Components/Home/Slide";
import Title from "../Components/Home/Title";
import Quantity from "../Components/Home/Quantity";
import SubTitle from "../Components/SubTitle";
import Faculty from "../Components/Faculty/Faculty";
import Major from "../Components/Major/Major";

function Direction(props) {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Slide />
          <Title />
          <Quantity />
        </Route>
        <Route exact path="/faculty">
          <SubTitle title="Khoa" />
          <Faculty />
        </Route>
        <Route exact path="/major">
          <SubTitle title="Chuyên ngành" />
          <Major />
        </Route>
      </Switch>
    </div>
  );
}

export default Direction;
