const axios = require("axios");
const noteInitialState = {
  addFaculty: false,
  facultyEdit: {},
  addMajor: false,
  majorEdit: {},
  majorRecord: [],
  facultyKeyword: "",
};
const allReducer = (state = noteInitialState, action) => {
  switch (action.type) {
    case "CHANGE_ADD_FACULTY_STATUS":
      return { ...state, addFaculty: !state.addFaculty };
    case "GET_EDIT_FACULTY":
      return { ...state, facultyEdit: action.item };
    case "DELETE_FACULTY":
      axios({
        method: "delete",
        url: `http://localhost:8080/faculty/delete/${action.id}`,
      });
      return state;
    case "FACULTY_SEARCH":;
      return {...state,facultyKeyword: action.keyword };

    case "CHANGE_ADD_MAJOR_STATUS":
      return { ...state, addMajor: !state.addMajor };
    case "GET_EDIT_MAJOR":
      return { ...state, majorEdit: action.item };
    case "DELETE_MAJOR":
      axios({
        method: "delete",
        url: `http://localhost:8080/major/delete/${action.id}`,
      });
      return state;

    default:
      return state;
  }
};
var redux = require("redux");
var store = redux.createStore(allReducer);
export default store;
