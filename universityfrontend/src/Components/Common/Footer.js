import React from 'react'

function Footer(props) {
    return (
        <footer>
        <div className="container-fluid footer">
          <div className="row  mt-3 pt-3">
            <div className="col-12 ">
              <a href="/"><img className="img-fluid mt-4 d-block mx-auto " src="../image/a.png" width="80px" height="auto" alt=
              "logo" /></a>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-5 mx-auto">
              <ul className=" navbar-nav text-center mt-3 pb-3 d-flex flex-row justify-content-between slidefooter  ">
                <li className="nav-item">
                  <a className="nav-link  " href="../faculty">Khoa</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link  " href="../major">Chuyên ngành</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link " href="../subject">Môn học</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link " href="../teacher">Giảng viên</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link " href="../student">Sinh viên</a>
                </li>
                <li className="nav-item ">
                  <a className="nav-link " href="../scoring">Kết quả học tập</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
      
    )
}
export default Footer

