import React from 'react'

function Header(props) {
    return (
      <header>
  <nav className="navbar navbar-expand-lg navbar-light navcolor navbar-fixed-top bg-faded ">
    <div className="container-fluid ">
      <a className="navbar-brand pl-5" href="/"><img className="img-fluid" src="../image/a.png" width="55px" height="auto"  alt="logo"/></a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse " id="navbarSupportedContent">
        <ul className="navbar-nav ml-auto  ">
          <li className="nav-item"><a className="nav-link textcolor " href="../faculty">Khoa</a>
          </li>
          <li className="nav-item"><a className="nav-link textcolor " href="../major">Chuyên ngành</a>
          </li>
          <li className="nav-item"><a className="nav-link textcolor" href="../subject">Môn học</a>
          </li>
          <li className="nav-item"><a className="nav-link textcolor" href="../teacher">Giảng viên</a>
          </li>
          <li className="nav-item"><a className="nav-link textcolor" href="../student">Sinh viên</a>
          </li>
          <li className="nav-item "><a className="nav-link textcolor" href="../scoring">Kết quả học tập</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>


    
      
    )
}
export default Header

