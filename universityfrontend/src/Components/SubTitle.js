import React from "react";
import PropTypes from "prop-types";

function SubTitle(props) {
  return (
    <div className="container">
      <div className="row">
      <div className="col-sm-12">
        <i className="fas fa-book-open iconsize d-block text-center" />
        <h1 className="text-center mt-1 mb-4">{props.title}</h1>
      </div>
    </div>
    </div>
  );
}

SubTitle.propTypes = {
    title: PropTypes.string.isRequired,
};
SubTitle.defaultProps= {
    title: '',
}

export default SubTitle;
