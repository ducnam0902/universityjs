import React, { useEffect, useState } from "react";
import Search from "./Search";
import { connect } from "react-redux";
import FacultyForm from "./FacultyForm";
import FacultyItems from "./FacultyItems";

const axios = require("axios");

function Faculty(props) {
  const [facultyData, setfacultyData] = useState([]);

  useEffect(() => {
    if (props.facultyKeyword === "") {
      axios({
        method: "get",
        url: "http://localhost:8080/faculty",
      }).then(function (response) {
        setfacultyData(response.data);
      });
    } else if (props.facultyKeyword.length > 0) {
      axios({
        method: "get",
        url: `http://localhost:8080/faculty/search/${props.facultyKeyword}`,
      }).then(function (response) {
        setfacultyData(response.data);
        return response.data;
      });
    } else setfacultyData([]);

    return () => {};
  }, [props.facultyKeyword]);

  const getFacultyRecords = () => {
    return facultyData.map((value, key) => {
      return <FacultyItems key={key} faculty={value} />;
    });
  };

  const FacultyInfo = () => {
    if (props.isAddFaculty) {
      return <FacultyForm />;
    } else
      return (
        <div>
          <Search />
          <div className="container">
            <table className="table table-bordered text-center" id="myTable">
              <thead className="tabletitle">
                <tr>
                  <th>Tên khoa</th>
                  <th>Địa chỉ</th>
                  <th>Số điện thoại</th>
                  <th>Cập nhật</th>
                </tr>
              </thead>
              <tbody>{getFacultyRecords()}</tbody>
            </table>
          </div>
        </div>
      );
  };
  return FacultyInfo();
}

const mapStateToProps = (state, ownProps) => {
  return {
    isAddFaculty: state.addFaculty,
    searchData: state.facultyRecord,
    facultyKeyword: state.facultyKeyword,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(Faculty);
