import React, { useState } from "react";
import { connect } from "react-redux";
const axios = require("axios");
function FacultyForm(props) {
  const [form, setform] = useState({
    facultyId: "",
    facultyName: "",
    address: "",
    phone: "",
  });
  const [data, setdata] = useState({
    facultyId: "",
    facultyName: "",
    address: "",
    phone: "",
  });

  const Cancel = () => {
    props.changeAddFacultyStatus();
  
  };

  var isChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setform({
      ...form,
    
      [name]: value,
    });
  };
  const add = () => {
   
    if (props.facultyEdit.facultyId) {
      form.facultyId= props.facultyEdit.facultyId;
      for(const property in form)
      if(form[property]=== "")
        {
          form[property]= props.facultyEdit.[property];
        }
      axios({
        method: "put",
        url: `http://localhost:8080/faculty/update/${props.facultyEdit.facultyId}`,
        data: form,
      });
    } else {
      axios({
        method: "post",
        url: "http://localhost:8080/faculty",
        data: form,
      });
    }
  };
  const functionTitle = () => {
    if (props.facultyEdit.facultyId){
        return "Cập nhật";
    }
     
    else return "Thêm mới";
  };
  return (
    <div className="container">
      <form>
        <div className=" form-group">
          <label>Tên khoa</label>
          <input
            type="text"
            className="form-control"
            name="facultyName"
            aria-describedby="helpId"
            placeholder="Nhập tên khoa"
            onChange={(event) => isChange(event)}
            defaultValue={props.facultyEdit.facultyName}
          />
          <label>Địa chỉ</label>
          <input
            type="text"
            className="form-control"
            name="address"
            aria-describedby="helpId"
            placeholder="Nhập địa chỉ khoa"
            onChange={(event) => isChange(event)}
            defaultValue={props.facultyEdit.address}
          />
          <label>Số điện thoại</label>
          <input
            type="text"
            className="form-control"
            name="phone"
            aria-describedby="helpId"
            placeholder="Nhập số điện thoại khoa"
            onChange={(event) => isChange(event)}
            defaultValue={props.facultyEdit.phone}
          />
        </div>
        <div className="d-flex justify-content-center">
          <a
            className="btn btn-success text-white mr-3"
            onClick={() => add()}
             href="/faculty"
          >
            {functionTitle()}
          </a>
          <button
            className="btn btn-secondary text-white"
            type="reset"
            onClick={() => Cancel()}
          >
            Hủy
          </button>
        </div>
      </form>
    </div>
  );
}
const mapStateToProps = (state, ownProps) => {
  return {
    facultyEdit: state.facultyEdit,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    changeAddFacultyStatus: () => {
      dispatch({ type: "CHANGE_ADD_FACULTY_STATUS" });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FacultyForm);
