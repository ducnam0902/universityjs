import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

function FacultyItems(props) {
  const editFaculty = () => { 
    props.sendEditFaculty(props.faculty);
    props.changeAddFacultyStatus();
  }
  const deleteFaculty = () => {
    props.sendDeleteFaculty(props.faculty.facultyId);
  }
  return (
    <tr>
      <td>{props.faculty.facultyName}</td>
      <td>{props.faculty.address}</td>
      <td>{props.faculty.phone}</td>
      <td>
        <a href="/faculty" className="btn btn-danger btn-sm mr-3" onClick={() => deleteFaculty()}>Xóa</a>
        <button className="btn btn-success btn-sm" onClick={ () => editFaculty()}>Chỉnh sửa</button>
      </td>
    </tr>
  );
}

FacultyItems.propTypes = {
  faculty: PropTypes.object.isRequired,
};
const mapStateToProps = (state, ownProps) => {
  return {
   
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    sendEditFaculty: (item) => {
      dispatch({type: "GET_EDIT_FACULTY", item})
    },
    changeAddFacultyStatus: () => {
      dispatch({type: "CHANGE_ADD_FACULTY_STATUS"})
    },
    sendDeleteFaculty: (id) => {
      dispatch({type: "DELETE_FACULTY", id})
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(FacultyItems);