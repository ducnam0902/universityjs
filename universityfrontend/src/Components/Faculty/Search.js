import React, { useState } from "react";
import { connect } from "react-redux";

function Search(props) {

  const [keyword, setkeyword] = useState("");

  const addFaculty = () => {
    props.changeAddFacultyStatus();
  };

  var isChange = (event) => {
    const value = event.target.value;
    setkeyword(value);
  };

  var facultySearch = (event) => {
    event.preventDefault();
    if(keyword){
       props.getFacultyKeyword(keyword);
    }
   }

  return (
    <div className="container">
      <div className="row my-3">
        <div className="col-sm-10">
          <form className="form-inline">
            <input
              className="form-control col-sm-6"
              type="search"
              name="keyword"
              placeholder=" Tìm kiếm"
              onChange={(event) => isChange(event)}
              required
            />
            <button type="Submit" className="btn btn-secondary mx-3 rounded" onClick={(event) => facultySearch(event) }>
              <i className="fas fa-search" />
              Tìm kiếm
            </button>
          </form>
        </div>
        <div className="col-sm-2 text-right">
          <button className="btn btn-primary" onClick={() => addFaculty()}>
            <i className="fas fa-plus" />
            Thêm mới
          </button>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    prop: state.prop,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    changeAddFacultyStatus: () => {
      dispatch({ type: "CHANGE_ADD_FACULTY_STATUS" });
    },
    getFacultyKeyword:  (keyword) => { 
      dispatch({ type: "FACULTY_SEARCH",keyword});
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Search);
