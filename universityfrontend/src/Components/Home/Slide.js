import React from "react";

function Slide(props) {
  return (
    <section className="slide">
      <div className="container-fluid">
        <div className="row">
          <div
            id="carouselExampleIndicators"
            className="carousel slide"
            data-ride="carousel"
          >
            <ol className="carousel-indicators">
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to={0}
                className="active"
              />
              <li data-target="#carouselExampleIndicators" data-slide-to={1} />
              <li data-target="#carouselExampleIndicators" data-slide-to={2} />
            </ol>
            <div className="carousel-inner imgslide ">
              <div className="carousel-item active ">
                <img
                  src="image/c.jpg"
                  className="d-block w-100 img-fluid"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src="image/d.png"
                  className="d-block w-100 img-fluid"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src="image/e.jpg"
                  className="d-block w-100 img-fluid"
                  alt="..."
                />
              </div>
            </div>
            <a
              className="carousel-control-prev"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="prev"
            >
              {" "}
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              />{" "}
              <span className="sr-only">Previous</span>
            </a>{" "}
            <a
              className="carousel-control-next"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="next"
            >
              {" "}
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              />{" "}
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Slide;
