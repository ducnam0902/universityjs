import React from 'react'

function Title(props) {
    return (
        <section className="container">
        <div className="row pt-5 text-center d-block">
          <i className="fas fa-book-open iconsize pb-3" />
          <h2 className="title text-uppercase text-secondary">Hệ thống quản lý đại học</h2>
        </div>
      </section>
    )
}
export default Title

