import React from "react";
import PropTypes from "prop-types";

function QuantityItem(props) {
  return (
    <div className="col-sm-4">
      <div className="subquantity my-3">
        <h3>{props.title}</h3>
        <i className={"fas " +props.icon} />
      </div>
    </div>
  );
}

QuantityItem.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
};
QuantityItem.defaultProps={
    title:'',
    icon: '',
}

export default QuantityItem;
