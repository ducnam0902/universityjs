import React from 'react'
import QuantityItem from './QuantityItem'

function Quantity(props) {
    return (
        <div className="container">
            <div className="row d-flex">
                <QuantityItem title="Khoa" icon="fa-university"/>
                <QuantityItem title="Chuyên ngành" icon="fa-book-reader" />
                <QuantityItem title="Sinh viên" icon="fa-user-graduate"/>
            </div>
            <div className="row d-flex">
                <QuantityItem title="Giảng viên" icon="fa-chalkboard-teacher"/>
                <QuantityItem title="Môn học" icon="fa-atlas"/>
                <QuantityItem title="Học kì" icon="fa-chalkboard"/>
            </div>
        </div>
    )
}
export default Quantity

