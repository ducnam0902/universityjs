import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
const axios = require("axios");
function Search(props) {
  const [keyword, setkeyword] = useState("");
  const [searchData, setsearchData] = useState([]);
  const addMajor = () => {
    props.changeAddMajorStatus();
  };
  var isChange = (event) => {
    const value = event.target.value;
    setkeyword(value);
  };
  var majorSearch = (event) => {
    event.preventDefault();
    if (keyword) {
      axios({
        method: "get",
        url: `http://localhost:8080/major/search/${keyword}`,
      }).then(function (response) {
        setsearchData(response.data);
        return response.data;
      });
      props.sendSearchData(searchData);
    }
  };
  return (
    <div className="container">
      <div className="row my-3">
        <div className="col-sm-10">
          <form className="form-inline">
            <input
              className="form-control col-sm-6"
              type="search"
              name="keyword"
              placeholder=" Tìm kiếm"
              onChange={(event) => isChange(event)}
              required
            />
            <button
              type="Submit"
              className="btn btn-secondary mx-3 rounded"
              onClick={(event) => majorSearch(event)}
            >
              <i className="fas fa-search" />
              Tìm kiếm
            </button>
          </form>
        </div>
        <div className="col-sm-2 text-right">
          <button className="btn btn-primary" onClick={() => addMajor()}>
            <i className="fas fa-plus" />
            Thêm mới
          </button>
        </div>
      </div>
    </div>
  );
}
Search.propTypes = {};
const mapStateToProps = (state, ownProps) => {
  return {
    prop: state.prop,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    changeAddMajorStatus: () => {
      dispatch({ type: "CHANGE_ADD_MAJOR_STATUS" });
    },
    getMajorKeyword: (keyword) => {
      dispatch({ type: "MAJOR_SEARCH", keyword });
    },
    sendSearchData: (searchData) => {
      dispatch({ type: "MAJOR_SEARCH_RESULTS", searchData });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Search);
