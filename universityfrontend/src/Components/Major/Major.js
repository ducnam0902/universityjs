import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import MajorForm from "./MajorForm";
import Search from "./Search";
import MajorItems from "./MajorItems";
import { connect } from "react-redux";

const axios = require("axios");
function Major(props) {
    const [majorData, setmajorData] = useState([]);

    useEffect(() => {
      axios({
        method: "get",
        url: "http://localhost:8080/major",
      }).then(function (response) {
        setmajorData(response.data);
      });
      return () => {
      }
    },[]);
    
    const getMajorRecords = () => {
      if (props.searchData.length > 0){
  
        return props.searchData.map((value, key) => {
          return <MajorItems key={key} major={value} />;
        });
      }
      else
        return majorData.map((value, key) => {
          return <MajorItems key={key} major={value} />;
        });
    };
  const MajorInfo = () => {
    if (props.isAddMajor) {
        return <MajorForm />;
      } else
        return (
          <div>
            <Search />
            <div className="container">
              <table className="table table-bordered text-center" id="myTable">
                <thead className="tabletitle">
                  <tr>
                    <th>Tên chuyên ngành</th>
                    <th>Tên khoa</th>
                    <th>Địa chỉ</th>
                    <th>Cập nhật</th>
                  </tr>
                </thead>
                <tbody>{getMajorRecords()}</tbody>
              </table>
            </div>
          </div>
        );
  };
  return MajorInfo();
}

Major.propTypes = {};
const mapStateToProps = (state, ownProps) => {
  return {
    isAddMajor: state.addMajor,
    searchData: state.majorRecord,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    // dispatch1: () => {
    //     dispatch(actionCreator)
    // }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Major);
