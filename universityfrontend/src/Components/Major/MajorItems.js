import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

function MajorItems(props) {
    const editMajor = () => { 
        props.sendEditMajor(props.major);
        props.changeAddMajorStatus();
      }
      const deleteMajor = () => {
        props.sendDeleteMajor(props.major.majorId);
      }
      return (
        <tr>
          <td>{props.major.majorName}</td>
          <td>{props.major.faculty.facultyName}</td>
          <td>{props.major.faculty.address}</td>
          <td>
            <a href="/major" className="btn btn-danger btn-sm mr-3" onClick={() => deleteMajor()}>Xóa</a>
            <button className="btn btn-success btn-sm" onClick={ () => editMajor()}>Chỉnh sửa</button>
          </td>
        </tr>
      );
}

MajorItems.propTypes = {
  major: PropTypes.object.isRequired,
}
const mapStateToProps = (state, ownProps) => {
    return {
     
    }
  }
  const mapDispatchToProps = (dispatch, ownProps) => {
    return {
      sendEditMajor: (item) => {
        dispatch({type: "GET_EDIT_MAJOR", item})
      },
      changeAddMajorStatus: () => {
        dispatch({type: "CHANGE_ADD_MAJOR_STATUS"})
      },
      sendDeleteMajor: (id) => {
        dispatch({type: "DELETE_MAJOR", id})
      }
    }
  }
  export default connect(mapStateToProps, mapDispatchToProps)(MajorItems);

