import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
const axios = require("axios");
function MajorForm(props) {
    const [form, setform] = useState({
        majorId: "",
        majorName: "",
        faculty: "",
      });
      const [faculty, setfaculty] = useState([]);
      useEffect(() => {
        axios({
            method: "get",
            url: "http://localhost:8080/faculty",
          }).then(function (response) {
            setfaculty(response.data);
          });
          return () => {
          }
      }, []);
    
      const Cancel = () => {
        props.changeAddMajorStatus();
      };
    
      var isChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setform({
          ...form,
        
          [name]: value,
        });
      };
      const add = () => {
       
        if (props.majorEdit.majorId) {
          form.majorId= props.majorEdit.majorId;
          for(const property in form)
          if(form[property]=== "")
            {
              form[property]= props.majorEdit.[property];
            }
          axios({
            method: "put",
            url: `http://localhost:8080/major/update/${props.majorEdit.majorId}`,
            data: form,
          });
        } else {
          axios({
            method: "post",
            url: "http://localhost:8080/major",
            data: form,
          });
        }
      };
      const functionTitle = () => {
        if (props.majorEdit.majorId) return "Cập nhật";
        else return "Thêm mới";
      };
      return (
        <div className="container">
          <form>
            <div className=" form-group">
              <label>Tên chuyên ngành</label>
              <input
                type="text"
                className="form-control"
                name="majorName"
                aria-describedby="helpId"
                placeholder="Nhập tên chuyên ngành"
                onChange={(event) => isChange(event)}
                defaultValue={props.majorEdit.majorName}
              />
              <label>Khoa quản lý</label>
              <select  class="form-control" name="faculty"  onChange={(event) => isChange(event)}>
                          {faculty.map((value, key) => {
                              return (
                                <option key={key} value={value.facultyId} >{value.facultyName}</option>
                              )
                          })}
                      </select>
            </div>
            <div className="d-flex justify-content-center">
              <a
                className="btn btn-success text-white mr-3"
                onClick={() => add()}
                 href="/major"
              >
                {functionTitle()}
              </a>
              <button
                className="btn btn-secondary text-white"
                type="reset"
                onClick={() => Cancel()}
              >
                Hủy
              </button>
            </div>
          </form>
        </div>
      );
   
}

const mapStateToProps = (state, ownProps) => {
    return {
      majorEdit: state.majorEdit,
    };
  };
  const mapDispatchToProps = (dispatch, ownProps) => {
    return {
      changeAddMajorStatus: () => {
        dispatch({ type: "CHANGE_ADD_MAJOR_STATUS" });
      },
    };
  };
  export default connect(mapStateToProps, mapDispatchToProps)(MajorForm);

